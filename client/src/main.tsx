import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { Alert, BlueCard, Button, Card, Column, Form, Row, AlertInput } from './widgets';
import { Component } from 'react-simplified';
import whiteboardService, { Subscription } from './whiteboard-service';
import { HashRouter, Route } from 'react-router-dom';
import { createHashHistory } from 'history';
import { User, ChatRoom, ChatRoomUserMap, Message } from '../../server/src/mesasage-services';

const history = createHashHistory();

export class Main extends Component<{ match: { params: { id: number } } }> {
  user: User = { username: '', password: '', id: 0 };
  subscription: Subscription | null = null;
  connected = false;
  message_input: string = '';
  messages: Message[] = [];
  chatRooms: ChatRoom[] = [];
  new_chatRoom_input: string = '';
  chatRoomId: number = 0;
  users: User[] = [];
  alertToggle: boolean = false;
  focusedUserList: boolean = false;
  userSearchInput = '';
  chatRoomUsers: { username: string }[] = [];

  render() {
    return (
      <>
        <div style={{ display: 'flex', flexDirection: 'column', height: 'calc(100vh - 240px)' }}>
          <Row>
            <Column width={10}>
              <h1>Messenger, logget inn som {this.user.username}</h1>
            </Column>
            <Column right>
              <Button.Light onClick={() => this.homeButton()}>Home</Button.Light>
            </Column>
          </Row>
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <Card title="Chatrooms">
              <div
                style={{
                  height: 'calc(100vh - 240px)',
                  width: '15vw',
                  overflowY: 'auto',
                  flexDirection: 'column',
                  display: 'flex',
                }}
              >
                <Card title="New chatRoom">
                  <Form.Input
                    placeholder="Title"
                    type="text"
                    width={'10vh'}
                    value={this.new_chatRoom_input}
                    onChange={(event) => {
                      this.new_chatRoom_input = event.currentTarget.value;
                    }}
                    onKeyDown={(event: any) => {
                      if (event.key == 'Enter') this.createChatRoomButton();
                    }}
                  ></Form.Input>
                  <Button.Success onClick={() => this.createChatRoomButton()}>
                    Create
                  </Button.Success>
                </Card>

                <Card onClick={() => (this.chatRoomId = 0)}>Main</Card>
                {this.chatRooms.map((chatRoom) => (
                  <Card
                    key={chatRoom.id}
                    onClick={() => {
                      this.chatRoomId = chatRoom.id;
                    }}
                  >
                    <Row>
                      <Column>{chatRoom.name}</Column>
                      <Column right>
                        {this.chatRoomId == chatRoom.id ? (
                          <>
                            <Button.Light
                              onClick={() => {
                                this.alertToggle = true;
                                whiteboardService.send({
                                  action: 'get_chatRoom_users',
                                  content: { chatRoomId: this.chatRoomId },
                                });
                              }}
                            >
                              ➕
                            </Button.Light>
                            {this.alertToggle && (
                              <AlertInput>
                                <div>
                                  <span
                                    className="close-btn"
                                    onClick={() => {
                                      this.alertToggle = false;
                                    }}
                                  >
                                    &times;
                                  </span>
                                  <Card title={`Add user to ${chatRoom.name}`}>
                                    <Row>
                                      <p>
                                        Current members:{' '}
                                        {this.chatRoomUsers.map((user) => user.username).join(', ')}
                                      </p>
                                    </Row>
                                    <Row>
                                      <Column>
                                        <Form.Input
                                          type="text"
                                          placeholder="username"
                                          value={this.userSearchInput}
                                          onChange={(event) => {
                                            this.userSearchInput = event.currentTarget.value;
                                            this.users.filter((user) =>
                                              user.username
                                                .toLowerCase()
                                                .includes(this.userSearchInput.toLowerCase()),
                                            ).length < 10
                                              ? (this.focusedUserList = true)
                                              : (this.focusedUserList = false);
                                          }}
                                          onFocus={() =>
                                            this.users.filter((user) =>
                                              user.username
                                                .toLowerCase()
                                                .includes(this.userSearchInput.toLowerCase()),
                                            ).length < 10
                                              ? (this.focusedUserList = true)
                                              : null
                                          }
                                        ></Form.Input>
                                      </Column>
                                    </Row>
                                    <Row>
                                      <Column>
                                        {this.focusedUserList ? (
                                          <>
                                            {this.users
                                              .filter((user) =>
                                                user.username
                                                  .toLowerCase()
                                                  .includes(this.userSearchInput.toLowerCase()),
                                              )
                                              .map((user) => (
                                                <Row key={user.id}>
                                                  <Column>{user.username} </Column>
                                                  <Column right>
                                                    <Button.Success
                                                      onClick={() =>
                                                        this.addToChatRoom(chatRoom.id, user.id)
                                                      }
                                                    >
                                                      Add user
                                                    </Button.Success>
                                                  </Column>
                                                </Row>
                                              ))}
                                          </>
                                        ) : (
                                          <p>Write more to search</p>
                                        )}
                                      </Column>
                                    </Row>
                                  </Card>
                                </div>
                              </AlertInput>
                            )}
                          </>
                        ) : null}
                      </Column>
                    </Row>
                  </Card>
                ))}
              </div>
            </Card>
            <Card
              title={`Meldinger i ${
                this.chatRooms.length != 0 && this.chatRoomId != 0
                  ? this.chatRooms
                    ? this.chatRooms.find((room) => room.id == this.chatRoomId)?.name
                    : this.chatRoomId
                  : 'Main'
              }`}
            >
              <div
                style={{
                  marginLeft: '1vw',
                  marginRight: '1vw',
                  height: 'calc(100vh - 240px)',
                  display: 'flex',
                  flexDirection: 'column-reverse',
                  overflowY: 'auto',
                  width: '78vw',
                }}
              >
                {this.messages
                  .filter((message) => message.message_group == this.chatRoomId)
                  .map((message, index) => (
                    <Row key={index}>
                      {message.sender == this.user.id ? (
                        <Column right>
                          <BlueCard
                            title={
                              this.users.length != 0
                                ? this.users.find((user) => message.sender == user.id)?.username
                                : null
                            }
                          >
                            {message.message}
                          </BlueCard>
                        </Column>
                      ) : (
                        <Column>
                          <Card
                            title={
                              this.users.length != 0
                                ? this.users.find((user) => message.sender == user.id)?.username
                                : null
                            }
                          >
                            {message.message}
                          </Card>
                        </Column>
                      )}
                    </Row>
                  ))}
              </div>
            </Card>
          </div>

          <div
            style={{
              flex: 1,
              display: 'flex',
              flexDirection: 'column-reverse',
              overflowY: 'auto',
            }}
          >
            <Card
              title={'send melding'}
              style={{ height: '110px', position: 'absolute', bottom: 0, left: 0, right: 0 }}
            >
              <Row>
                <Column>
                  <Form.Input
                    type="text"
                    value={this.message_input}
                    onChange={(event) => {
                      this.message_input = event.currentTarget.value;
                    }}
                    width={window.innerWidth - 130}
                    onKeyDown={(event: any) => {
                      if (event.key == 'Enter') this.sendButton();
                    }}
                  ></Form.Input>
                </Column>
                <Column right>
                  <Button.Success
                    onClick={() => {
                      this.sendButton();
                    }}
                  >
                    Send
                  </Button.Success>
                </Column>
              </Row>
            </Card>
          </div>
        </div>
      </>
    );
  }

  addToChatRoom(chatRoomId: number, userId: number) {
    this.focusedUserList = false;

    whiteboardService.send({
      action: 'add_user_to_chatroom',
      content: {
        chatRoomId: chatRoomId,
        userId: userId,
      },
    });
    this.userSearchInput = '';
  }

  homeButton() {
    history.push(`/`);
    whiteboardService.send({
      action: 'logg_out',
      content: '',
    });
    if (this.subscription) whiteboardService.unsubscribe(this.subscription);
  }

  createChatRoomButton() {
    whiteboardService.send({
      action: 'chatroom_create',
      content: {
        title: this.new_chatRoom_input,
        userId: this.user.id,
      },
    });
    this.new_chatRoom_input = '';
  }
  sendButton() {
    whiteboardService.send({
      action: 'message_send',
      content: {
        message: this.message_input,
        sender: this.user.id,
        message_group: this.chatRoomId,
      },
    });
    this.message_input = '';
  }

  mounted(): void {
    this.subscription = whiteboardService.subscribe();

    this.subscription.onopen = () => {
      this.connected = true;
      whiteboardService.send({
        action: 'user_get',
        content: this.props.match.params.id,
      });
      whiteboardService.send({
        action: 'message_get',
        content: '',
      });
      whiteboardService.send({
        action: 'chatrooms_get',
        content: { userId: '' },
      });
      whiteboardService.send({
        action: 'chatrooms_user_get',
        content: { userId: this.user.id },
      });
    };

    this.subscription.onmessage = (message) => {
      let instance = Alert.instance();
      if (instance) instance.alerts = [];
      if (message.action == 'user_data') {
        this.user = message.content;
      }
      if (message.action == 'users_data') {
        this.users = message.content;
      }
      if (message.action == 'user_data_failed') {
        history.push(`/`);
      }
      if (message.action == 'message_receive') {
        this.messages = [...message.content].reverse();
      }
      if (message.action == 'chatrooms_recieve') {
        this.chatRooms = message.content;

        this.render();
      }
      if (message.action == 'receive_chatRoom_users') {
        this.chatRoomUsers = message.content;
        console.log(this.chatRoomUsers);
        this.render();
      }

      if (message.action == 'error') {
        console.error(message.content);
        Alert.danger(message.content);
      }
    };
  }
}
