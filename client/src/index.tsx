import * as React from 'react';
import { createRoot } from 'react-dom/client';
import { Alert } from './widgets';
import { HashRouter, Route } from 'react-router-dom';
import { UserLogin } from './userLogin';
import { Main } from './main';

let root = document.getElementById('root');
if (root)
  createRoot(root).render(
    <HashRouter>
      <>
        <Route exact path="/" component={UserLogin} />
        <Route exact path="/user/:id" component={Main} />
        <Alert />
      </>
    </HashRouter>,
  );
