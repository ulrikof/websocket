import * as React from 'react';
import { Alert, BlueCard, Button, Card, Column, Form, Row, AlertInput } from './widgets';
import { Component } from 'react-simplified';
import whiteboardService, { Subscription } from './whiteboard-service';
import { createHashHistory } from 'history';
import axios from 'axios';
import loginServices from './message-http-services';

const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

export class UserLogin extends Component {
  username: string = '';
  password: string = '';
  subscription: Subscription | null = null;
  connected = false;
  login_create_toggle: boolean = true;

  render() {
    return this.login_create_toggle ? (
      <Card title="Log into user">
        <Form.Input
          placeholder="Username"
          type="text"
          value={this.username}
          onChange={(event) => {
            this.username = event?.currentTarget.value;
          }}
        ></Form.Input>
        <Form.Input
          placeholder="Password"
          type="password"
          value={this.password}
          onChange={(event) => {
            this.password = event?.currentTarget.value;
          }}
        ></Form.Input>
        <Button.Success onClick={() => this.loginButton()}>Log in</Button.Success>
        <Button.Light onClick={() => this.toggleLoginButton()}>create user</Button.Light>
      </Card>
    ) : (
      <Card title="Create new user">
        <Form.Input
          placeholder="Username"
          type="text"
          value={this.username}
          onChange={(event) => {
            this.username = event?.currentTarget.value;
          }}
        ></Form.Input>
        <Form.Input
          placeholder="Password"
          type="password"
          value={this.password}
          onChange={(event) => {
            this.password = event?.currentTarget.value;
          }}
        ></Form.Input>
        <Button.Success onClick={() => this.createButton()}>create new user</Button.Success>
        <Button.Light onClick={() => this.toggleLoginButton()}>log into user</Button.Light>
      </Card>
    );
  }

  toggleLoginButton() {
    this.login_create_toggle = !this.login_create_toggle;
  }
  async loginButton() {
    // whiteboardService.send({
    //   action: 'user_login',
    //   content: { username: this.username, password: this.password },
    // });
    try {
      const response = await loginServices.user_login(this.username, this.password);
      console.log(response);
      //@ts-ignore
      history.push(`/user/${response.data.cookie}`);
      if (this.subscription) whiteboardService.unsubscribe(this.subscription);
    } catch (error) {
      console.error(error);
    }
  }
  createButton() {
    whiteboardService.send({
      action: 'user_create',
      content: { username: this.username, password: this.password },
    });
  }

  mounted(): void {
    this.subscription = whiteboardService.subscribe();

    // Called when the subscription is ready
    this.subscription.onopen = () => {
      this.connected = true;
    };
    // Called on incoming message
    this.subscription.onmessage = (message) => {
      if (message.action == 'user_success') {
        history.push(`/user/${message.content}`);
        if (this.subscription) whiteboardService.unsubscribe(this.subscription);
        let instance = Alert.instance();
        if (instance) instance.alerts = [];
      }
      if (message.action == 'user_login_failed') {
        Alert.danger(message.content);
      }
    };
  }
}
