import axios from 'axios';

axios.defaults.baseURL = 'http://10.22.221.97:3000/api/v1/whiteboard';

class LoginServices {
  async user_login(username: string, password: string) {
    try {
      const response = await axios.post<{ data: { cookie: number } }>('/#/', {
        username,
        password,
      });
      return response;
    } catch (error: unknown) {
      throw error;
    }
  }
}

const loginServices = new LoginServices();
export default loginServices;
