import pool from './mysql-pool';
import type { RowDataPacket, ResultSetHeader } from 'mysql2';

export type User = {
  id: number;
  username: string;
  password: string;
};

export type Message = {
  id: number;
  message: string;
  sender: number;
  message_group: number;
};

export type ChatRoom = {
  id: number;
  name: string;
};

export type ChatRoomUserMap = {
  user_id: number;
  chatroom_id: number;
};
class UserService {
  get(id: number) {
    return new Promise<User | undefined>((resolve, reject) => {
      pool.query('SELECT * FROM users WHERE id = ?', [id], (error, results: RowDataPacket[]) => {
        if (error) return reject(error);
        if (results.length == 0) reject('user not found');
        resolve(results[0] as User);
      });
    });
  }

  login(username: string, password: string) {
    return new Promise<User | undefined>((resolve, reject) => {
      pool.query(
        'SELECT * FROM users WHERE username = ? AND password = ?',
        [username, password],
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results[0] as User);
        },
      );
    });
  }

  /**
   * Get all tasks.
   */
  getAll() {
    return new Promise<User[]>((resolve, reject) => {
      pool.query('SELECT * FROM users', [], (error, results: RowDataPacket[]) => {
        if (error) return reject(error);

        resolve(results as User[]);
      });
    });
  }

  /**
   * Create new task having the given title.
   *
   * Resolves the newly created task id.
   */
  create(username: string, password: string) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO users (username, password) VALUES (?,?)',
        [username, password],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          resolve(results.insertId);
        },
      );
    });
  }

  getAllInChatRoom(chatRoomId: number) {
    return new Promise<User[]>((resolve, reject) => {
      pool.query(
        'SELECT users.username FROM user_chatroom join users on user_chatroom.user_id = users.id where user_chatroom.chatroom_id = ?',
        [chatRoomId],
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);
          if (results.length == 0) reject('user not found');
          resolve(results as User[]);
        },
      );
    });
  }
}

class MessageService {
  get(chatroom_id: number[]) {
    const placeholders = chatroom_id.map(() => '?').join(','); // this will create '?,?'
    return new Promise<Message[]>((resolve, reject) => {
      pool.query(
        `SELECT * FROM messages WHERE message_group in (${placeholders})`,
        chatroom_id,
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results as Message[]);
        },
      );
    });
  }

  getAll() {
    return new Promise<Message[]>((resolve, reject) => {
      pool.query('SELECT * FROM messages', [], (error, results: RowDataPacket[]) => {
        if (error) return reject(error);

        resolve(results as Message[]);
      });
    });
  }

  create(message: string, sender: number, message_group: number) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO messages (message, sender, message_group) VALUES (?,?,?)',
        [message, sender, message_group],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          resolve(results.insertId);
        },
      );
    });
  }
}

class ChatRoomService {
  get(id: number[]) {
    const placeholders = id.map(() => '?').join(','); // this will create '?,?'

    return new Promise<ChatRoom[]>((resolve, reject) => {
      pool.query(
        `SELECT * FROM chatrooms WHERE id in (${placeholders})`,
        id,
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results as ChatRoom[]);
        },
      );
    });
  }

  getAll() {
    return new Promise<ChatRoom[]>((resolve, reject) => {
      pool.query('SELECT * FROM chatrooms', [], (error, results: RowDataPacket[]) => {
        if (error) return reject(error);

        resolve(results as ChatRoom[]);
      });
    });
  }

  create(name: string) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO chatrooms (name) VALUES (?)',
        [name],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          resolve(results.insertId);
        },
      );
    });
  }
}

class ChatRoomUserService {
  get(userId: number) {
    return new Promise<ChatRoomUserMap[] | undefined>((resolve, reject) => {
      pool.query(
        'SELECT * FROM user_chatroom WHERE user_id = ?',
        [userId],
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results as ChatRoomUserMap[]);
        },
      );
    });
  }

  getAll() {
    return new Promise<ChatRoom[]>((resolve, reject) => {
      pool.query('SELECT * FROM user_chatroom', [], (error, results: RowDataPacket[]) => {
        if (error) return reject(error);

        resolve(results as ChatRoom[]);
      });
    });
  }

  create(user_id: number, chatroom_id: number) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO user_chatroom (user_id, chatroom_id) VALUES (?,?)',
        [user_id, chatroom_id],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          resolve(results.insertId);
        },
      );
    });
  }
}

export const chatRoomUserService = new ChatRoomUserService();
export const chatRoomService = new ChatRoomService();
export const userService = new UserService();
export const messageService = new MessageService();
