import type http from 'http';
import type https from 'https';
import WebSocket from 'ws';
import {
  userService,
  messageService,
  chatRoomService,
  chatRoomUserService,
} from './mesasage-services';
import express from 'express';

function getRandomNumber() {
  return Math.floor(Math.random() * 1000000000000000) + 1;
}

let user_auth: any = [];
let user_cookie: any = [];
/**
 * Whiteboard server
 */

const router = express.Router();

router.post('/', async (request, response) => {
  try {
    const user = await userService.login(request.body.username, request.body.password);
    if (!user) throw 'could not find user';
    // user_auth[user.id] = connection;
    const cookie = getRandomNumber();
    user_cookie[user.id] = cookie;
    // connection.send(JSON.stringify({ action: 'user_success', content: cookie }));
    response.send({ cookie: cookie });
  } catch (error) {
    response.status(400).send({ error: error });
  }
});

export { router };

export default class WhiteboardServer {
  /**
   * Constructs a WebSocket server that will respond to the given path on webServer.
   */
  constructor(webServer: http.Server | https.Server, path: string) {
    const server = new WebSocket.Server({ server: webServer, path: path + '/whiteboard' });

    server.on('connection', (connection, _request) => {
      connection.on('message', async (message) => {
        const data = JSON.parse(message.toString());

        if (data.action == 'user_login') {
          try {
            const user = await userService.login(data.content.username, data.content.password);
            if (!user) throw 'could not find user';
            user_auth[user.id] = connection;
            const cookie = getRandomNumber();
            user_cookie[user.id] = cookie;
            connection.send(JSON.stringify({ action: 'user_success', content: cookie }));
          } catch (error) {
            connection.send(
              JSON.stringify({ action: 'user_login_failed', content: 'could not find user' }),
            );
          }
        }
        if (data.action == 'user_create') {
          try {
            if (data.content.username == '' || data.content.password == '') {
              throw 'Username and password cant be empty';
            }
            let users = await userService.getAll();

            if (users.find((user) => user.username == data.content.username)) {
              throw 'Duplicate username';
            }
            let id = await userService.create(data.content.username, data.content.password);

            user_auth[id] = connection;
            const cookie = getRandomNumber();
            user_cookie[id] = cookie;
            connection.send(JSON.stringify({ action: 'user_success', content: id }));
          } catch (error) {
            connection.send(
              JSON.stringify({
                action: 'user_login_failed',
                content: `could not create user: ${error}`,
              }),
            );
          }
        }

        if (data.action == 'user_get') {
          try {
            const userId = user_cookie.indexOf(Number(data.content));
            user_auth[userId] = connection;

            const user = await userService.get(Number(user_auth.indexOf(connection)));

            if (user) {
              connection.send(JSON.stringify({ action: 'user_data', content: user }));
            } else throw 'user not found';
          } catch (error) {
            connection.send(
              JSON.stringify({
                action: 'user_data_failed',
                content: `could not fetch user: ${error}`,
              }),
            );
          }
        }

        if (data.action == 'user_get') {
          try {
            const user = await userService.getAll();
            connection.send(JSON.stringify({ action: 'users_data', content: user }));
          } catch (error) {
            connection.send(
              JSON.stringify({ action: 'user_data_failed', content: 'could not fetch users' }),
            );
          }
        }

        if (data.action == 'message_send') {
          try {
            messageService.create(
              data.content.message,
              data.content.sender,
              data.content.message_group,
            );

            // const messages = await messageService.getAll();

            server.clients.forEach(async (connection) => {
              const userId = user_auth.indexOf(connection);
              const chatroom_user_map = await chatRoomUserService.get(userId);
              if (chatroom_user_map) {
                const chatRoomIds = chatroom_user_map.map((item) => Number(item.chatroom_id));
                chatRoomIds.push(0);
                const messages = await messageService.get(chatRoomIds);
                connection.send(JSON.stringify({ action: 'message_receive', content: messages }));
              } else {
                connection.send(JSON.stringify({ action: 'error', content: 'cant get messages' }));
              }
            });
          } catch (error) {
            server.clients.forEach((connection) =>
              connection.send(
                JSON.stringify({ action: 'error', content: 'Unable to recive messages' }),
              ),
            );
          }
        }

        if (data.action == 'message_get') {
          try {
            const userId = user_auth.indexOf(connection);
            const chatroom_user_map = await chatRoomUserService.get(userId);
            if (chatroom_user_map) {
              const chatRoomIds = chatroom_user_map.map((item) => Number(item.chatroom_id));
              chatRoomIds.push(0);
              const messages = await messageService.get(chatRoomIds);
              connection.send(JSON.stringify({ action: 'message_receive', content: messages }));
            } else {
              connection.send(JSON.stringify({ action: 'error', content: 'cant get messages' }));
            }

            // const messages = await messageService.getAll();
          } catch (error) {
            connection.send(
              JSON.stringify({ action: 'error', content: 'Unable to recive messages' }),
            );
          }
        }

        if (data.action == 'chatroom_create') {
          try {
            if (data.content.title == '') throw 'ChatRoom title cant be empty';
            const chatroomId = await chatRoomService.create(data.content.title);
            await chatRoomUserService.create(data.content.userId, chatroomId);

            const chatroom_user_map = await chatRoomUserService.get(data.content.userId);
            if (chatroom_user_map) {
              const userIds = chatroom_user_map.map((item) => Number(item.chatroom_id));
              let chatrooms = await chatRoomService.get(userIds);
              connection.send(JSON.stringify({ action: 'chatrooms_recieve', content: chatrooms }));
            } else throw 'could not get user_ids';
          } catch (error) {
            connection.send(
              JSON.stringify({ action: 'error', content: `Unable to create chatroom, ${error}` }),
            );
          }
        }
        if (data.action == 'chatrooms_get') {
          try {
            const chatroom_user_map = await chatRoomUserService.get(user_auth.indexOf(connection));
            if (chatroom_user_map) {
              if (chatroom_user_map.length == 0) {
                return null;
              }
              const userIds = chatroom_user_map.map((item) => Number(item.chatroom_id));
              let chatrooms = await chatRoomService.get(userIds);
              connection.send(JSON.stringify({ action: 'chatrooms_recieve', content: chatrooms }));
            } else throw 'could not get user_ids';
          } catch (error) {
            connection.send(
              JSON.stringify({ action: 'error', content: `Unable to fetch chatrooms, ${error}` }),
            );
          }
        }
        if (data.action == 'chatrooms_user_get') {
          try {
            const chatroom_user_map = await chatRoomUserService.get(data.content.userId);

            connection.send(
              JSON.stringify({ action: 'chatrooms_user_receive', content: chatroom_user_map }),
            );
          } catch (error) {
            connection.send(
              JSON.stringify({
                action: 'error',
                content: `Unable to get chatroom_user_map, ${error}`,
              }),
            );
          }
        }

        if (data.action == 'add_user_to_chatroom') {
          try {
            await chatRoomUserService.create(data.content.userId, data.content.chatRoomId);
            let chatroom_user_map = await chatRoomUserService.get(data.content.userId);
            if (chatroom_user_map) {
              const userIds = chatroom_user_map.map((item) => Number(item.chatroom_id));
              let chatrooms = await chatRoomService.get(userIds);
              let user_connection = user_auth[data.content.userId];

              if (user_connection) {
                user_connection.send(
                  JSON.stringify({ action: 'chatrooms_recieve', content: chatrooms }),
                );
              }
              const users = await userService.getAllInChatRoom(data.content.chatRoomId);
              connection.send(JSON.stringify({ action: 'receive_chatRoom_users', content: users }));
            }
          } catch (error) {
            connection.send(
              JSON.stringify({ action: 'error', content: 'Unable to add user to chatroom' }),
            );
          }
        }

        if (data.action == 'get_chatRoom_users') {
          try {
            const users = await userService.getAllInChatRoom(data.content.chatRoomId);
            connection.send(JSON.stringify({ action: 'receive_chatRoom_users', content: users }));
          } catch (error) {
            connection.send(
              JSON.stringify({ action: 'error', content: 'Unable to get users in chatroom' }),
            );
          }
        }
        if (data.action == 'logg_out') {
          // const index = user_auth.indexOf(connection);
          // delete user_auth[index];
        }
        // Send the message to all current client connections
      });
    });
  }
}
